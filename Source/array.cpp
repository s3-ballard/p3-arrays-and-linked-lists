//
//  array.cpp
//  CommandLineTool
//
//  Created by Scott Ballard on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "array.hpp"

Array::Array()
{
    numElements = 0;
    floatPoint = nullptr;
}

Array::~Array()
{
    delete[] floatPoint;
}

void Array::add (float itemValue)
{
    //create a new pointer array that is one larger then the last item in the list
    float* temp = new float[size()+1];
    
    //loop through items in the temporary array and move pointer through them
    for (int counter = 0; counter < size(); counter++)
        temp[counter] = floatPoint[counter];

    //the last element in the temporary array is the entered item
    temp[size()] = itemValue;
   
    //if float point does not equal anything then delete the memory array
    if (floatPoint != nullptr)
        //deletes already exisiting values
        delete[] floatPoint;
    
    //pointer stores all the elements of the temporary array;
    floatPoint = temp;
    
    //increase number of elements.
    numElements++;
}

float Array::get (int index) const
{
    return floatPoint[index];
}

int Array::size()
{
    return numElements;
}
