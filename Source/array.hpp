//
//  array.hpp
//  CommandLineTool
//
//  Created by Scott Ballard on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef array_hpp
#define array_hpp

#include <stdio.h>

class Array
{
    
public:
    /** constructer*/
    Array();
    
    /** destructor */
    ~Array();
    
    /** Adds new elements into the array */
    void add (float itemValue);
    
    /** returns item at index */
    float get (int index) const;
    
    /** returns number of items in an array */
    int size();
    
private:
    int numElements;
    float *floatPoint;
    
    
};

#endif /* array_hpp */
