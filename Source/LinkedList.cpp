//
//  LinkedList.cpp
//  CommandLineTool
//
//  Created by Scott Ballard on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "LinkedList.hpp"

LinkedList:: LinkedList()
{
    head = nullptr;
    current = nullptr;

}


LinkedList:: ~LinkedList()
{
    delete[]head;
    delete[]current;
}


void LinkedList:: add(float itemValue)
{
    //create a temporary node
    Node* temp = new Node;
    
    //the value of the node is the argument
    temp->value = itemValue;
    //initialise the node to null
    temp->next = nullptr;
    
    //if the first pointer is null
    if (head == nullptr)
    {
        head = temp;
    }
    //if the head is already chosen
    else if (head != nullptr)
    {
        //assign the current position to the head node
        current = head;
        
        while (current->next != nullptr)
        {
            //progress through until the next current pointer location is null
            current = current->next;
        }
        
        //the next node will contain the temporary information
        current->next = temp;
    }


}

float LinkedList:: get(int index)
{
    //move the current pointer to the location of the head pointer
    current = head;
    
    //loop until the counter reaches the index number
    for (int counter = 0; counter != index; counter++)
    {
        //move the current pointer to the next location
        current = current->next;
       
    }
    //return the value of the current pointer
    return current->value;
    
}

int LinkedList:: size()
{
    int sizeOf = 0;
    //move the current pointer to the beginning
    current = head;
    
    //while the current is not pointed a null value, increase the sizeOf variable
    while (current != nullptr)
        
    {
        current = current->next;
        sizeOf++;
    }
    
    return sizeOf;
    
}
