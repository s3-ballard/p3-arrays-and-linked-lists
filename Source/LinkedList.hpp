//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Scott Ballard on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>


class LinkedList
{

public:

    LinkedList();
    
    ~LinkedList();
    
    void add(float itemValue);
    
    float get(int index);
    
    int size();
    
private:
    struct Node
    {
        float value;
        Node* next;
    };
    
    Node* head;
    Node* current;


};
#endif /* LinkedList_hpp */
